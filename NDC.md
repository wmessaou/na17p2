# Note de Clarification
***
Git du projet n°7 : Dashboard.
URL du projet : https://gitlab.utc.fr/wmessaou/na17p2/

### Livrables
***
*   README.md (Nom/Prenom et Sujet du projet)
*   NDC au format markdown
*   MCD
*   MLD relationnel
*   BBD : base de données, données de test, questions

### Membre du projet
***
*   MESSAOUDI Walid

### Objet du projet
***
Le projet Dashboard a pour objectif la création d'un tableau de bord collaboratif regroupant des flux de publications. Un flux de publication centralise
les publications d'un même domaine. 

### Fonctions spécialisées
***
On proposera au moins les quatre vues suivantes :
*   **Vue trie_flux** : permet de trier les flux de publications du score le plus élevé au plus bas
*   **Vue affichage_information_utilisateur** : permet d'afficher le nom de l'utilisateur, le prénom de l'utilisateur; l'e-mail de l'utilisateur, les groupes auxquels appartient l'utilisateur et un dashboard lui présentant tous les flux qu'il peut lire
*   **Vue affichage_commentaire** : permet d'afficher les commentaires du plus anciens au plus récent d'une publication
*   **Vue affichage_publication** : permet d'afficher les publications d'un flux qui ont été validé par le responsable du flux
*   **Vue affichage_publication_responsableFlux** : permet d'afficher les publications d'un flux qui ont été ou non validé par le responsable du flux
*   **Vue trie_flux_date** : permet de trier les flux de publications du plus vieux au plus ancien

### Liste des objets qui devront être gérés dans la base de données
***
*   **Utilisateur**
*   **Flux de publication**
*   **Publication** (Publication graphique, Publication textuelle)
*   **Groupe d'utilisateur** (Rédacteur, Lecteurs)
*   **Commentaire**
*   **Entreprise**
*   **Domaine**
*   **Modele Economique**
*   **Salaire**

### Liste des propriétés associées à chaque objet
***
##### Utilisateur
- Email
- Nom
- Prenom
- Age
- Date de naissance
- Entreprise
- Genre
- Pays
- Ville
- Code postale
- Metier
- Flux d'appartenance
- Groupe d'appartenance
- Commentaire publié
- Publication
- **Contrainte :**  
    - **Un utilisateur ne peut voter qu'une fois par publication** 
    - **Un utilisateur étant dans la catégorie « Rédacteur » et « Lecteur » se verra attribué le plus fort privilège : « Rédacteur »**
    - **Seul un utilisateur appartenant au groupe Rédacteur peut créer une publication**

##### Entreprise
- Nom 
- Siege_social    
- Description    
- Domaine
- CEO   

##### Domaine
- Label
- Description

##### Flux De Publication
- Titre
- Responsable
- Créateur
- Confidentialité
- Groupe d'utilisateur
- Publication
- Score
- **Contrainte :**  
    - **Chaque flux possède au minimum un Groupe Utilisateur « Rédacteur », et un « Lecteur »** 
    - **Seul le responsable de flux peut valider / dévalider / rejeter / supprimer / modifier tous les articles de son flux**

##### Publication
- Titre
- Groupe d'appartenance
- Flux d'appartenance
- Date de publication
- Créateur
- Donnée
- Valider
- Commentaire
- **Contrainte :**  
    - **Un article n'est visible qu'une fois validé** 
    - **Dans l'état « validé », l'article ne peut être modifié**
    - **Si une publication a un score négatif (Like - Dislike < 0), elle devient automatiquement « rejetée »**
    - **Chaque Rédacteur peut modifier / supprimer ses propres publications**
    - **Après toute modification par le responsable, d'une publication, celui-ci devient le rédacteur de la publication**
    - **Une publication est soit Textuelle soit graphique**
    - **Valider est par défaut à False**

##### Groupe d'Utilisateur
- Nom
- Responsable
- Créateur
- Nombre d'utilisateur
- Utilisateur
- Flux d'appartenance
- Publication

##### Commentaire
- Titre
- Date de publication
- Nombre de like
- Nombre de dislike
- Publication d'appartenance
- Créateur
- Contenue
- **Contrainte :**
    - **Un commentaire ne peut être publié que dans les flux auxquels appartient le créateur du commentaire**
    - **Les commentaires d'une publication apparaissent du plus ancien au plus récent**
    - **Un commentaire peut être modifié/supprimer uniquement par le créateur**

##### Salaire
- brut : number
- part_patronale : number
- part_salariale : number

##### Modele Economique
- tresorerie    
- partenaires
- pointsVentes
- EntrepriseClientes

### Liste des fonctions que ces utilisateurs pourront effectuer
***
**Les lecteurs, Rédacteurs, responsables de flux et responsables de groupe sont aussi des utilisateurs**

##### Utilisateur
- Tout utilisateur peut créer des groupes
- Tout utilisateur peut créer son flux de publication
- Chaque utilisateur d'un flux peut poster des commentaires concernant une publication
- Chaque utilisateur d'un flux peut « Like » ou « Dislike » une publication des flux auxquels il appartient
- Tout utilisateur peut trier les flux de publications du score le plus élevé au plus bas 
- Peut consulter ses informations personnelles (nom, prénom, email, groupes auxquels il appartient, dashboard)

##### Rédacteur
- Chaque Rédacteur peut créer une publication
- Chaque Rédacteur peut modifier / supprimer ses propres publications
- Chaque Rédacteur peut lire les publications des flux auxquels il appartient

##### Lecteur
- Chaque Lecteur peut lire les publications des flux auxquels il appartient

##### Responsable de flux
- Il peut valider / dévalider / rejeter / supprimer / modifier tous les articles de son flux
- Il peut transférer la responsabilité à un autre utilisateur.
- Il peut définir quels sont les groupes « Redacteurs » et « Lecteurs »

##### Responsable de groupe
- Il peut transférer sa responsabilité
- Il peut ajouter ou supprimer des utilisateurs du groupe

### Liste des hypothèses faites à partir du sujet
***
- **Hypothèse 1** :Le score d'une publication correspond à la différence Like / Dislike
- **Hypothèse 2** :Si une publication a un score négatif (Like - Dislike < 0), elle devient automatiquement « rejetée »
- **Hypothèse 3** :Le score d'un flux correspond à la moyenne des scores de chaque publication
- **Hypothèse 4** :Si le responsable de flux délègue la responsabilité a quelqu'un d'autre il peut néanmoins rester responsable des groupes « Lecteurs » et « Rédacteurs »
- **Hypothèse 5** :Le responsable de flux n'est pas obligatoirement le responsable des groupes « Lecteurs » et « Rédacteurs »
- **Hypothèse 6** :Les utilisateurs peuvent consulter les publications provenant des groupes auxquels ils appartiennent
- **Hypothèse 7** :Un utilisateur peut lire les publications d'un flux s'il fait partie de ce flux et qu'il appartient au groupe « lecteurs » et/ou rédacteur
- **Hypothèse 8** :Un utilisateur peut publier un article dans un flux s'il appartient au flux et s'il fait partie du groupe « rédacteur »
- **Hypothèse 9** :Un commentaire peut être modifié/ supprimer par son créateur 
- **Hypothèse 10** :Une publication ne pourra être publié qu'après validation du responsable de flux
- **Hypothèse 11** :Le rédacteur initial d'une publication est le créateur de la publication
- **Hypothèse 12** :Une publication ne peut être modifié que par le rédacteur de la publication et le responsable de flux. 
- **Hypothèse 13** :Si le responsable de flux modifie une publication, il en devient le rédacteur.
- **Hypothèse 14** :Lorsqu'un responsable de flux transfert sa responsabilité, il reste membre du flux et appartient toujours aux groupes « rédacteurs » et « lecteurs »
- **Hypothèse 15** :Lorsqu'un responsable de groupe transfert sa responsabilité, il reste membre du groupe
- **Hypothèse 16** :Une publication est soit de type « textuelle » soit de type « graphique » mais ne peut être les deux à la fois 
- **Hypothèse 17** :La confidentialité d'un flux est soit privée soit publique
- **Hypothèse 18** :Les groupes d'utilisateur ne peuvent avoir le même nom au sein d'un même flux
- **Hypothèse 29** :Les flux de publications ne peuvent avoir le même nom 
- **Hypothèse 20** :Les publications ne peuvent avoir le même nom au sein d'un même flux


### Liste des suggestions faites à partir du sujet
***
- Il existe deux types de publications possible dans un Dashboard :
    - Publication textuelle 
    - Publication graphique

