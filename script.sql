--Create

-- Table: public.Domaine

CREATE TABLE IF NOT EXISTS public."Domaine"
(
  label text  NOT NULL,
  id_label SERIAL,
  description text ,
  CONSTRAINT "Domaine_pkey" PRIMARY KEY (id_label)
);

-- Table: public.Entreprise

CREATE TABLE IF NOT EXISTS public."Entreprise"
(
    nom_entreprise text  NOT NULL,
    siege_social text  NOT NULL,
    description text ,
    domaine smallint,
    modeleEconomique JSON NOT NULL,
    CONSTRAINT "Entreprise_pkey" PRIMARY KEY (nom_entreprise),
    CONSTRAINT "Entreprise_domaine_fkey" FOREIGN KEY (domaine)
        REFERENCES public."Domaine" (id_label)
);


-- Table: public.Utilisateur

CREATE TABLE IF NOT EXISTS public."Utilisateur"
(
  email text  NOT NULL,
  nom character varying(30)  NOT NULL,
  prenom character varying(30)  NOT NULL,
  date_de_naissance date NOT NULL,
  genre "char" NOT NULL,
  pays character varying(30)  NOT NULL,
  ville character varying(30)  NOT NULL,
  code_postale integer NOT NULL,
  metier character varying(50)  NOT NULL,
  nom_entreprise text  NOT NULL,
  salaire JSON NOT NULL,
  CONSTRAINT "Utilisateur_pkey" PRIMARY KEY (email),
  CONSTRAINT nom_entreprise FOREIGN KEY (nom_entreprise)
  REFERENCES public."Entreprise" (nom_entreprise),
  CONSTRAINT genre CHECK (genre = 'H'::"char" OR genre = 'F'::"char")
);

-- Table: public.Flux_de_publication

CREATE TABLE public."Flux_de_publication"
(
    titre_flux text NOT NULL,
    date_de_creation date NOT NULL,
    description text,
    responsable text NOT NULL,
    createur text NOT NULL,
    label smallint NOT NULL,
    confidentialite character varying(8)[] NOT NULL,
    CONSTRAINT "Flux_de_publication_pkey" PRIMARY KEY (titre_flux),
    CONSTRAINT "Flux_de_publication_createur_fkey" FOREIGN KEY (createur)
        REFERENCES public."Utilisateur" (email),
    CONSTRAINT "Flux_de_publication_responsable_fkey" FOREIGN KEY (responsable)
        REFERENCES public."Utilisateur" (email),
    CONSTRAINT "Flux_de_publication_confidentialite_check" CHECK (confidentialite = '{Publique}'::character varying[] OR confidentialite = '{Privée}'::character varying[]) NOT VALID
);

-- Type: t

CREATE TYPE public."t" AS ENUM
    ('publication_graphique', 'publication_textuelle');

CREATE TABLE IF NOT EXISTS public."Publication"
(
  titre_publication text  NOT NULL,
  titre_flux text  NOT NULL,
  date_de_publication date NOT NULL,
  description text ,
  valider boolean NOT NULL DEFAULT false,
  "donnée" text  NOT NULL,
  type t NOT NULL,
  createur text  NOT NULL,
  CONSTRAINT "Publication_pkey" PRIMARY KEY (titre_publication, titre_flux),
  CONSTRAINT "Publication_createur_fkey" FOREIGN KEY (createur)
  REFERENCES public."Utilisateur" (email),
  CONSTRAINT "Publication_titre_flux_fkey" FOREIGN KEY (titre_flux)
  REFERENCES public."Flux_de_publication" (titre_flux)
);

-- Table: public."Commentaire"

CREATE TABLE IF NOT EXISTS public."Commentaire"
(
  id_commentaire SERIAL ,
  titre_commentaire text  NOT NULL,
  date_de_publication date NOT NULL,
  contenue text  NOT NULL,
  createur text  NOT NULL,
  titre_publication text  NOT NULL,
  titre_flux text  NOT NULL,
  CONSTRAINT "Commentaire_pkey" PRIMARY KEY (id_commentaire),
  CONSTRAINT "Commentaire_createur_fkey" FOREIGN KEY (createur)
  REFERENCES public."Utilisateur" (email),
  CONSTRAINT "Commentaire_titre_publication_titre_flux_fkey" FOREIGN KEY (titre_flux, titre_publication)
  REFERENCES public."Publication" (titre_flux, titre_publication)
);

-- Table: public.Groupe_utilisateur

CREATE TABLE IF NOT EXISTS public."Groupe_utilisateur"
(
  nom_groupe text  NOT NULL,
  date_de_creation date NOT NULL,
  description text ,
  responsable text  NOT NULL,
  createur text  NOT NULL,
  CONSTRAINT "Groupe_utilisateur_pkey" PRIMARY KEY (nom_groupe),
  CONSTRAINT "Groupe_utilisateur_createur_fkey" FOREIGN KEY (createur)
  REFERENCES public."Utilisateur" (email),
  CONSTRAINT "Groupe_utilisateur_responsable_fkey" FOREIGN KEY (responsable)
  REFERENCES public."Utilisateur" (email)
);

-- Table: public.Lecteurs

CREATE TABLE IF NOT EXISTS public."Lecteurs"
(
  nom_groupe text  NOT NULL,
  titre_flux text  NOT NULL,
  CONSTRAINT "Lecteurs_pkey" PRIMARY KEY (nom_groupe, titre_flux),
  CONSTRAINT "Lecteurs_nom_groupe_fkey" FOREIGN KEY (nom_groupe)
  REFERENCES public."Groupe_utilisateur" (nom_groupe),
  CONSTRAINT "Lecteurs_titre_flux_fkey" FOREIGN KEY (titre_flux)
  REFERENCES public."Flux_de_publication" (titre_flux)
);

-- Table: public.Redacteurs

CREATE TABLE IF NOT EXISTS public."Redacteurs"
(
  nom_groupe text  NOT NULL,
  titre_flux text  NOT NULL,
  CONSTRAINT "Redacteurs_pkey" PRIMARY KEY (nom_groupe, titre_flux),
  CONSTRAINT "Redacteurs_nom_groupe_fkey" FOREIGN KEY (nom_groupe)
  REFERENCES public."Groupe_utilisateur" (nom_groupe),
  CONSTRAINT "Redacteurs_titre_flux_fkey" FOREIGN KEY (titre_flux)
  REFERENCES public."Flux_de_publication" (titre_flux)
);

-- Table: public.Utilisateur_FaitPartie_Flux

CREATE TABLE IF NOT EXISTS public."Utilisateur_FaitPartie_Flux"
(
  utilisateur text  NOT NULL,
  titre_flux text  NOT NULL,
  CONSTRAINT "Utilisateur_FaitPartie_Flux_pkey" PRIMARY KEY (utilisateur, titre_flux),
  CONSTRAINT "Utilisateur_FaitPartie_Flux_titre_flux_fkey" FOREIGN KEY (titre_flux)
  REFERENCES public."Flux_de_publication" (titre_flux) ,
  CONSTRAINT "Utilisateur_FaitPartie_Flux_utilisateur_fkey" FOREIGN KEY (utilisateur)
  REFERENCES public."Utilisateur" (email)
);

-- Table: public.Utilisateur_FaitPartie_Groupe

CREATE TABLE IF NOT EXISTS public."Utilisateur_FaitPartie_Groupe"
(
  nom_groupe text  NOT NULL,
  email text  NOT NULL,
  CONSTRAINT "Utilisateur_FaitPartie_Groupe_pkey" PRIMARY KEY (nom_groupe, email),
  CONSTRAINT "Utilisateur_FaitPartie_Groupe_email_fkey" FOREIGN KEY (email)
  REFERENCES public."Utilisateur" (email) ,
  CONSTRAINT "Utilisateur_FaitPartie_Groupe_nom_groupe_fkey" FOREIGN KEY (nom_groupe)
  REFERENCES public."Groupe_utilisateur" (nom_groupe)
);

-- Table: public.Utilisateur_dislike_Publication

CREATE TABLE IF NOT EXISTS public."Utilisateur_dislike_Publication"
(
  utilisateur text  NOT NULL,
  titre_publication text  NOT NULL,
  titre_flux text  NOT NULL,
  CONSTRAINT "Utilisateur_dislike_Publication_pkey" PRIMARY KEY (utilisateur, titre_publication, titre_flux),
  CONSTRAINT "Utilisateur_dislike_Publicati_titre_publication_titre_flux_fkey" FOREIGN KEY (titre_flux, titre_publication)
  REFERENCES public."Publication" (titre_flux, titre_publication) ,
  CONSTRAINT "Utilisateur_dislike_Publication_utilisateur_fkey" FOREIGN KEY (utilisateur)
  REFERENCES public."Utilisateur" (email)
);

-- Table: public.Utilisateur_like_Publication

CREATE TABLE IF NOT EXISTS public."Utilisateur_like_Publication"
(
  utilisateur text  NOT NULL,
  titre_publication text  NOT NULL,
  titre_flux text  NOT NULL,
  CONSTRAINT "Utiliteur_like_Publication_pkey" PRIMARY KEY (utilisateur, titre_publication),
  CONSTRAINT "Utiliteur_like_Publication_titre_publication_titre_flux_fkey" FOREIGN KEY (titre_flux, titre_publication)
  REFERENCES public."Publication" (titre_flux, titre_publication) ,
  CONSTRAINT "Utiliteur_like_Publication_utilisateur_fkey" FOREIGN KEY (utilisateur)
  REFERENCES public."Utilisateur" (email)
);

--Insert

INSERT INTO public."Domaine" VALUES ('HighTech', 1, 'entia non sunt multiplicanda praeter necessitatem');
INSERT INTO public."Domaine" VALUES ('Téléphonie', 2, 'entia non sunt multiplicanda praeter necessitatem');
INSERT INTO public."Domaine" VALUES ('Television', 3, 'entia non sunt multiplicanda praeter necessitatem');
INSERT INTO public."Domaine" VALUES ('Pc', 4, 'entia non sunt multiplicanda praeter necessitatem');
INSERT INTO public."Domaine" VALUES ('Composants informatiques ', 5, 'entia non sunt multiplicanda praeter necessitatem');


INSERT INTO public."Entreprise" VALUES ('Apple', 'Californie', 'entia non sunt multiplicanda praeter necessitatem', 2,'{"tresorerie":10200000000,"partenaires":["Samsung","IBM","GE"],"pointsVentes":["Internet","Magasins"],"entrepriseClientes":["Google","Evian","CoolerMaster"]}');
INSERT INTO public."Entreprise" VALUES ('Samsung', 'Taiwan', 'entia non sunt multiplicanda praeter necessitatem', 1,'{"tresorerie":8800000000,"partenaires":["Apple","Sharp"],"pointsVentes":["Internet","Magasins"],"entrepriseClientes":["Nike","Nitendo"]}');
INSERT INTO public."Entreprise" VALUES ('Sharp', 'Osaka', 'entia non sunt multiplicanda praeter necessitatem', 3,'{"tresorerie":2000000000,"partenaires":["EPC","JiraService"],"pointsVentes":["Internet","Magasins"],"entrepriseClientes":["MyProtein","ASUS"]}');
INSERT INTO public."Entreprise" VALUES ('Acer', 'Taiwan', 'entia non sunt multiplicanda praeter necessitatem', 4,'{"tresorerie":4000000000,"partenaires":["AMD","NVIDIA"],"pointsVentes":["Internet","Magasins"],"entrepriseClientes":["Nivea","Bic"]}');
INSERT INTO public."Entreprise" VALUES ('Msi', 'Taiwan', 'entia non sunt multiplicanda praeter necessitatem', 5,'{"tresorerie":1500000000,"partenaires":["Microsoft","Google"],"pointsVentes":["Internet","Magasins"],"entrepriseClientes":["AMD","Huawei","Obs"]}');


INSERT INTO public."Utilisateur" VALUES ('utilisateur1@gmail.com', 'Rotsh', 'Macron', '1984-10-05', 'H', 'France', 'Epinay', 93800, 'Réparateur ', 'Apple','{"brut":3210, "part_patronale":610, "part_salariale":2600}');
INSERT INTO public."Utilisateur" VALUES ('utilisateur2@gmail.com', 'Palim', 'Mounir', '1998-10-07', 'H', 'Algerie', 'Alger', 92140, 'Ingénieur', 'Sharp','{"brut":4210, "part_patronale":600, "part_salariale":2400}');
INSERT INTO public."Utilisateur" VALUES ('utilisateur3@gmail.com', 'Juviano', 'Hugo', '2001-10-05', 'H', 'France', 'Villetaneuse', 93400, 'Testeur', 'Sharp','{"brut":5210, "part_patronale":1000, "part_salariale":2300}');
INSERT INTO public."Utilisateur" VALUES ('utilisateur4@gmail.com', 'Robben', 'Arjen', '1982-11-02', 'H', 'Pays-Bas', 'Amsterdam', 91520, 'Developpeur', 'Msi','{"brut":1210, "part_patronale":605, "part_salariale":2200}');
INSERT INTO public."Utilisateur" VALUES ('utilisateur5@gmail.com', 'Mbappe', 'Kyllian', '1998-05-10', 'H', 'France', 'Paris', 75000, 'Ingénieur', 'Msi','{"brut":2567, "part_patronale":610, "part_salariale":3100}');
INSERT INTO public."Utilisateur" VALUES ('utilisateur6@gmail.com', 'Cristiano', 'Ronaldo', '1997-08-02', 'H', 'Portugal', 'Lisbone', 93124, 'Testeur', 'Apple','{"brut":3542, "part_patronale":610, "part_salariale":1800}');
INSERT INTO public."Utilisateur" VALUES ('utilisateur7@gmail.com', 'Messi', 'Lionel', '1978-01-01', 'H', 'Argentine', 'Buenos Aires', 58412, 'Ingénieur', 'Apple','{"brut":5340, "part_patronale":800, "part_salariale":900}');
INSERT INTO public."Utilisateur" VALUES ('utilisateur8@gmail.com', 'Verrati', 'Marco', '1994-10-05', 'H', 'Italie', 'Rome', 21457, 'Ingénieur', 'Msi','{"brut":2100, "part_patronale":400, "part_salariale":770}');
INSERT INTO public."Utilisateur" VALUES ('utilisateur9@gmail.com', 'Pires', 'David', '1968-10-08', 'H', 'France', 'Limoge', 52873, 'Developpeur', 'Samsung','{"brut":2200, "part_patronale":400, "part_salariale":4561}');
INSERT INTO public."Utilisateur" VALUES ('utilisateur10@gmail.com', 'Platini', 'Michele', '1965-07-05', 'H', 'France', 'Enghien Les Bains', 95000, 'Ingénieur', 'Acer','{"brut":2230, "part_patronale":500, "part_salariale":1600}');
INSERT INTO public."Utilisateur" VALUES ('utilisateur11@gmail.com', 'Torres', 'Fernando', '1978-06-02', 'H', 'Espagne', 'Madrid', 47689, 'Testeur', 'Sharp','{"brut":2300, "part_patronale":400, "part_salariale":2000}');
INSERT INTO public."Utilisateur" VALUES ('utilisateur12@gmail.com', 'Neymar', 'Junior', '1992-10-03', 'H', 'Bresile', 'Brasilia', 45678, 'Developpeur', 'Samsung','{"brut":2700, "part_patronale":500, "part_salariale":2200}');
INSERT INTO public."Utilisateur" VALUES ('utilisateur13@gmail.com', 'Rashford', 'Marcus', '1964-10-03', 'H', 'Angleterre ', 'Londres', 415, 'Ingénieur', 'Samsung','{"brut":1900, "part_patronale":500, "part_salariale":1000}');
INSERT INTO public."Utilisateur" VALUES ('utilisateur14@gmail.com', 'Can', 'Emre', '2000-11-05', 'H', 'Allemagne', 'Berlin', 78963, 'Developpeur', 'Acer','{"brut":1950, "part_patronale":380, "part_salariale":1550}');
INSERT INTO public."Utilisateur" VALUES ('utilisateur15@gmail.com', 'Anelka', 'Nicolas', '1999-11-05', 'H', 'France', 'Rouen', 74123, 'Ingénieur', 'Acer','{"brut":2000, "part_patronale":400, "part_salariale":1600}');


INSERT INTO public."Flux_de_publication" VALUES ('Iphone12', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur7@gmail.com', 'utilisateur7@gmail.com', 2, '{Privée}');
INSERT INTO public."Flux_de_publication" VALUES ('MsiKeyboard', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur8@gmail.com', 'utilisateur8@gmail.com', 5, '{Publique}');
INSERT INTO public."Flux_de_publication" VALUES ('Tele16k', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur2@gmail.com', 'utilisateur2@gmail.com', 3, '{Publique}');
INSERT INTO public."Flux_de_publication" VALUES ('ChromeBook', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur15@gmail.com', 'utilisateur15@gmail.com', 4, '{Publique}');
INSERT INTO public."Flux_de_publication" VALUES ('RobotAspirateur', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur13@gmail.com', 'utilisateur13@gmail.com', 1, '{Publique}');


INSERT INTO public."Publication" VALUES ('SpecIphone', 'Iphone12', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', true, 'entia non sunt multiplicanda praeter necessitatem', 'publication_textuelle', 'utilisateur6@gmail.com');
INSERT INTO public."Publication" VALUES ('PrixIphone', 'Iphone12', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', false, 'entia non sunt multiplicanda praeter necessitatem', 'publication_textuelle', 'utilisateur1@gmail.com');
INSERT INTO public."Publication" VALUES ('SpecRobot', 'RobotAspirateur', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', true, 'entia non sunt multiplicanda praeter necessitatem', 'publication_textuelle', 'utilisateur12@gmail.com');
INSERT INTO public."Publication" VALUES ('PrixRobot', 'RobotAspirateur', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', true, 'entia non sunt multiplicanda praeter necessitatem', 'publication_textuelle', 'utilisateur9@gmail.com');
INSERT INTO public."Publication" VALUES ('SpecTele', 'Tele16k', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', true, 'entia non sunt multiplicanda praeter necessitatem', 'publication_textuelle', 'utilisateur3@gmail.com');
INSERT INTO public."Publication" VALUES ('PrixTele', 'Tele16k', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', false, 'entia non sunt multiplicanda praeter necessitatem', 'publication_textuelle', 'utilisateur2@gmail.com');
INSERT INTO public."Publication" VALUES ('SpecChromeBook', 'ChromeBook', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', true, 'entia non sunt multiplicanda praeter necessitatem', 'publication_textuelle', 'utilisateur15@gmail.com');
INSERT INTO public."Publication" VALUES ('PrixChromeBook', 'ChromeBook', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', true, 'entia non sunt multiplicanda praeter necessitatem', 'publication_textuelle', 'utilisateur14@gmail.com');
INSERT INTO public."Publication" VALUES ('SpecKeyboard', 'MsiKeyboard', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', false, 'entia non sunt multiplicanda praeter necessitatem', 'publication_textuelle', 'utilisateur4@gmail.com');
INSERT INTO public."Publication" VALUES ('PrixKeyboard', 'MsiKeyboard', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', true, 'entia non sunt multiplicanda praeter necessitatem', 'publication_textuelle', 'utilisateur8@gmail.com');


INSERT INTO public."Commentaire" VALUES (1, 'Trop cher !', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur10@gmail.com', 'PrixChromeBook', 'ChromeBook');
INSERT INTO public."Commentaire" VALUES (2, 'Pas mal pour le prix', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur4@gmail.com', 'PrixKeyboard', 'MsiKeyboard');
INSERT INTO public."Commentaire" VALUES (3, 'Un peu cher..', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur12@gmail.com', 'PrixRobot', 'RobotAspirateur');
INSERT INTO public."Commentaire" VALUES (4, 'Pas mal les caractéristiques !', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur11@gmail.com', 'SpecTele', 'Tele16k');
INSERT INTO public."Commentaire" VALUES (5, 'Encore un capteur photo, ca fait beaucoup la non ?', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur1@gmail.com', 'SpecIphone', 'Iphone12');


INSERT INTO public."Groupe_utilisateur" VALUES ('RedacteurApple', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur7@gmail.com', 'utilisateur7@gmail.com');
INSERT INTO public."Groupe_utilisateur" VALUES ('LecteurApple', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur7@gmail.com', 'utilisateur7@gmail.com');
INSERT INTO public."Groupe_utilisateur" VALUES ('RedacteurSharp', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur2@gmail.com', 'utilisateur2@gmail.com');
INSERT INTO public."Groupe_utilisateur" VALUES ('LecteurSharp', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur2@gmail.com', 'utilisateur2@gmail.com');
INSERT INTO public."Groupe_utilisateur" VALUES ('RedacteurMSI', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur8@gmail.com', 'utilisateur8@gmail.com');
INSERT INTO public."Groupe_utilisateur" VALUES ('LecteurMSI', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur8@gmail.com', 'utilisateur8@gmail.com');
INSERT INTO public."Groupe_utilisateur" VALUES ('RedacteurAcer', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur15@gmail.com', 'utilisateur15@gmail.com');
INSERT INTO public."Groupe_utilisateur" VALUES ('LecteurAcer', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur15@gmail.com', 'utilisateur15@gmail.com');
INSERT INTO public."Groupe_utilisateur" VALUES ('RedacteurSamsung', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur13@gmail.com', 'utilisateur13@gmail.com');
INSERT INTO public."Groupe_utilisateur" VALUES ('LecteurSamsung', '2020-06-03', 'entia non sunt multiplicanda praeter necessitatem', 'utilisateur13@gmail.com', 'utilisateur13@gmail.com');


INSERT INTO public."Lecteurs" VALUES ('LecteurAcer', 'ChromeBook');
INSERT INTO public."Lecteurs" VALUES ('LecteurMSI', 'MsiKeyboard');
INSERT INTO public."Lecteurs" VALUES ('LecteurApple', 'Iphone12');
INSERT INTO public."Lecteurs" VALUES ('LecteurSamsung', 'RobotAspirateur');
INSERT INTO public."Lecteurs" VALUES ('LecteurSharp', 'Tele16k');


INSERT INTO public."Redacteurs" VALUES ('RedacteurAcer', 'ChromeBook');
INSERT INTO public."Redacteurs" VALUES ('RedacteurApple', 'Iphone12');
INSERT INTO public."Redacteurs" VALUES ('RedacteurMSI', 'MsiKeyboard');
INSERT INTO public."Redacteurs" VALUES ('RedacteurSamsung', 'RobotAspirateur');
INSERT INTO public."Redacteurs" VALUES ('RedacteurSharp', 'Tele16k');


INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur1@gmail.com', 'Iphone12');
INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur2@gmail.com', 'Tele16k');
INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur3@gmail.com', 'Tele16k');
INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur4@gmail.com', 'MsiKeyboard');
INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur5@gmail.com', 'MsiKeyboard');
INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur6@gmail.com', 'Iphone12');
INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur7@gmail.com', 'Iphone12');
INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur8@gmail.com', 'MsiKeyboard');
INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur9@gmail.com', 'RobotAspirateur');
INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur10@gmail.com', 'ChromeBook');
INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur11@gmail.com', 'Tele16k');
INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur12@gmail.com', 'RobotAspirateur');
INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur13@gmail.com', 'RobotAspirateur');
INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur14@gmail.com', 'ChromeBook');
INSERT INTO public."Utilisateur_FaitPartie_Flux" VALUES ('utilisateur15@gmail.com', 'ChromeBook');


INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurApple', 'utilisateur1@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurSharp', 'utilisateur2@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurSharp', 'utilisateur3@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurMSI', 'utilisateur4@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('LecteurMSI', 'utilisateur5@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurApple', 'utilisateur6@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('LecteurApple', 'utilisateur7@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurMSI', 'utilisateur8@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurSamsung', 'utilisateur9@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('LecteurAcer', 'utilisateur10@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('LecteurSharp', 'utilisateur11@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurSamsung', 'utilisateur12@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('LecteurSamsung', 'utilisateur13@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurAcer', 'utilisateur14@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurAcer', 'utilisateur15@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurMSI', 'utilisateur5@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurApple', 'utilisateur7@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurAcer', 'utilisateur10@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurSharp', 'utilisateur11@gmail.com');
INSERT INTO public."Utilisateur_FaitPartie_Groupe" VALUES ('RedacteurSamsung', 'utilisateur13@gmail.com');


INSERT INTO public."Utilisateur_dislike_Publication" VALUES ('utilisateur15@gmail.com', 'PrixChromeBook', 'ChromeBook');
INSERT INTO public."Utilisateur_dislike_Publication" VALUES ('utilisateur6@gmail.com', 'PrixIphone', 'Iphone12');


INSERT INTO public."Utilisateur_like_Publication" VALUES ('utilisateur10@gmail.com', 'PrixChromeBook', 'ChromeBook');
INSERT INTO public."Utilisateur_like_Publication" VALUES ('utilisateur15@gmail.com', 'PrixChromeBook', 'ChromeBook');
INSERT INTO public."Utilisateur_like_Publication" VALUES ('utilisateur7@gmail.com', 'PrixIphone', 'Iphone12');
INSERT INTO public."Utilisateur_like_Publication" VALUES ('utilisateur6@gmail.com', 'PrixIphone', 'Iphone12');


--Vues

--Affiche le nom de l'utilisateur, le prénom de l'utilisateur; l'e-mail de l'utilisateur, les groupes auxquels appartient l'utilisateur et les flux qu'il peut lire
CREATE VIEW view_affichage_information_utilisateur(nom,prenom,email,groupe,flux)
AS SELECT nom,prenom,public."Utilisateur".email,nom_groupe,titre_flux
FROM public."Utilisateur"
INNER JOIN public."Utilisateur_FaitPartie_Groupe" ON public."Utilisateur".email = public."Utilisateur_FaitPartie_Groupe".email
INNER JOIN public."Utilisateur_FaitPartie_Flux" ON public."Utilisateur".email = public."Utilisateur_FaitPartie_Flux".utilisateur;


--Affiche les publications d'un flux qui ont été validé
CREATE VIEW view_affichage_publication (publication,flux)
AS SELECT titre_publication, titre_flux FROM public."Publication" WHERE valider=true;


--Affiche les publications d'un flux qui ont été ou non validé
CREATE VIEW view_affichage_publication_responsableFlux (publication,flux)
AS SELECT titre_publication, titre_flux FROM public."Publication";


--Trie les commentaires d'une publication du plus ancien au plus récent
CREATE VIEW view_affichage_commentaire
AS SELECT * FROM public."Commentaire" ORDER BY date_de_publication;


-- Compte le nombre de like par publication
CREATE VIEW view_like_publication (nblike,titre_flux)
AS SELECT distinct count(public."Utilisateur_like_Publication".titre_flux),public."Flux_de_publication".titre_flux
FROM public."Flux_de_publication"
LEFT JOIN public."Utilisateur_like_Publication"
ON public."Flux_de_publication".titre_flux = public."Utilisateur_like_Publication".titre_flux GROUP BY public."Flux_de_publication".titre_flux ;


-- Compte le nombre de disklike par publication
CREATE VIEW view_dislike_publication (nbdislike,titre_flux)
AS SELECT distinct count(public."Utilisateur_dislike_Publication".titre_flux),public."Flux_de_publication".titre_flux
FROM public."Flux_de_publication"
LEFT JOIN public."Utilisateur_dislike_Publication"
ON public."Flux_de_publication".titre_flux = public."Utilisateur_dislike_Publication".titre_flux GROUP BY public."Flux_de_publication".titre_flux ;


-- Réuni le nombre de like et dislike par rapport à chaque flux
CREATE VIEW view_likeDislike_Flux
AS SELECT view_like_publication.nblike as nblike, view_dislike_publication.nbdislike as nbdislike, view_like_publication.titre_flux
FROM view_like_publication
LEFT JOIN view_dislike_publication ON view_like_publication.titre_flux=view_dislike_publication.titre_flux;


--Trie les flux de publications du score le plus elevé au score le plus faible
CREATE VIEW view_trie_flux
AS SELECT titre_flux, nblike-nbdislike AS score FROM view_likeDislike_Flux ORDER BY score DESC;


--Trie les flux de publications du plus vieux au plus ancien
CREATE VIEW view_trie_flux_date
AS SELECT * FROM public."Flux_de_publication" ORDER BY date_de_creation;
