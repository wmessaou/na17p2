# MLD 

### Informations

- Une clé artificielle a été rajouté dans la relation domaine car elle ne possedait pas de clés
- Une clé artificielle a été rajouté dans la relation commentaire car elle ne possedait pas de clés 
- Les relations N:M ont été nommées comme suit : "Nom_classe1"+"Nom_association"+"Nom_classe2".
- Nous considérons, implicite, le fait que les groupes Lecteurs et Redacteurs peuvent lire les publications

### Association N:M
***

**Utilisateur_FaitPartie_Groupe**(#nom_groupe=>Groupe_utilisateur, #utilisateur=>Utilisateur)
- **Contrainte** : PROJECTION(Utilisateur_FaitPartie_Groupe,nom_groupe) = PROJECTION(Groupe_utilisateur,nom_groupe)

**Utilisateur_like_Publication**(#utilisateur=>Utilisateur,#titre_publication=>Publication,#titre_flux=>Publication)
- **Contrainte** : Un utilisateur ne peut pas Like plusieurs fois une même publication

**Utilisateur_dislike_Publication**(#utilisateur=>Utilisateur,#titre_publication=>Publication,#titre_flux=>Publication)
- **Contrainte** : Un utilisateur ne peut pas dislike plusieurs fois une même publication

**Utilisateur_FaitPartie_Flux**(#utilisateur=>Utilisateur,#titre_flux=>Flux_de_Publication)
- **Contrainte** : PROJECTION(Utilisateur,email) = PROJECTION(Utilisateur_FaitPartie_Flux,utilisateur)

### Association 1:N
***

**Entreprise**(#nom_entreprise:text, siege_social:text, description:text, ceo:text, label=>Domaine)
siege_social NOT NULL, Ceo NOT NULL, label NOT NULL

**Domaine**(#id_label:number, label:text, description:text)
label NOT NULL

**Utilisateur**(#email:text, nom:varchar(30), prenom:varchar(30), date_de_naissance:Date, genre:{'H'|'F'}, pays:varchar(30), ville:varchar(30), code_postale:integer, metier:varchar(50), nom_entreprise=>Entreprise)
- **Contrainte** : Utilisateur.nom NOT NULL et PROJECTION(Entreprise,nom_entreprise) = PROJECTION(Utilisateur,nom_entreprise)
- nom NOT NULL, date_de_naissance NOT NULL, pays NOT NULL, ville NOT NULL, code_postale NOT NULL, metier NOT NULL

**Commentaire**(#id_commentaire:integer, titre_commentaire:text, date_de_publication:Date, contenue:String, createur=>Utilisateur,titre_publication=>Publication, titre_flux=>Publication)
- date_de_publication NOT NULL, contenue NOT NULL
- (titre, date_de_publication) clé candidate

**Publication**(#titre_publication:text, #titre_flux=>Flux_de_publication, date_de_publication:Date, description:text, valider:boolean, donnée:String, t:{publication_graphique,publication_textuelle}, créateur=>Utilisateur)
- date_de_publication NOT NULL, valider NOT NULL, donnee : NOT NULL
- **Contrainte :** Une publication ne peut êre créée que par un utilisateur appartenant au groupe Rédacteur du flux
- Si le score d'une publication est négatif alors la publication est suprimée

**Groupe_Utilisateur**(#nom_groupe:text, date_de_creation:Date, description:text, responsable=>Utilisateur,créateur=>Utilisateur)
- date_de_creation NOT NULL

**ModeleEconomique**(#id:number, #nom_entreprise=>Entreprise, tresorerie : number)

**Salaire**(#id:number, brut : decimal, part_patronale : decimal, part_salariale : decimal)
- brut, part_patronale, part_salariale NOT NULL

### Attributs multivalués 

**Partenaires**(#Partenaires:String, #nom_entreprise=>Entreprise, #modeleEconomique=>ModeleEconomique)

**pointsVentes**(#pointsVentes:String, #nom_entreprise=>Entreprise, #modeleEconomique=>ModeleEconomique)

**EntrepriseClientes**(#EntrepriseClientes:String, #nom_entreprise=>Entreprise, #modeleEconomique=>ModeleEconomique)

### Choix de l'héritage
***

**Héritage Publication :**

- La classe "Publication" est abstraite et l'héritage est complet.
- La classe mère Publication a des associations. L'héritage par classe filles est impossible.
- L'héritage par référence et par classe mère sont tous les deux adaptés à cette situation. Mais puisque l'héritage est complet alors on priviligira l'héritage par classe mère.
- La meilleure représentation est donc l'hériatge par classe mère.

**Héritage Groupe :**

- La classe "Groupe d'utilisateur" est abstraite
- L'héritage n'est pas complet car les classes filles ont des associations avec d'autres classes. L'héritage par classe mère n'est pas possible.
- La classe mère contient des associations. L'héritage par classe fille n'est pas possible.
- La meilleure représentation est donc l'héritage par référence.

### Contraintes héritages
***

**Héritage par référence (avec classe mère abstraite) :**
Contraintes : PROJECTION(Groupe_Utilisateur,nom_groupe) = PROJECTION(Lecteurs,nom_groupe) UNION PROJECTION(Rédacteurs,nom_groupe) UNION PROJECTION(Autre,nom_groupe)

**Héritage par classe mère (avec classe mère abstraite) :**

t NOT NULL


